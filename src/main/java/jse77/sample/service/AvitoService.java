package jse77.sample.service;

import jse77.sample.model.Item;
import jse77.sample.repository.ItemRepository;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Slf4j
@Service
public class AvitoService {

    private final ItemRepository itemRepository;

    @Autowired
    public AvitoService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public void proceedSearchItems() throws Exception {
        log.info("Starting new elements searching...");

        String url = "https://www.avito.ru/rossiya?q=ferrari";

        Document doc = Jsoup.connect(url)
                .userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)" +
                        " Chrome/85.0.4183.102 Safari/537.36")
                .get();

        Elements elements = doc.select("*[data-marker=\"item\"]");

        int count = 0;

        for (Element element : elements) {
            Item item = parseElement(element);

            itemRepository.save(item);
            count++;
        }

        log.info("saved or updated {} elements", count);
    }


    private Item parseElement(Element element) {
        Long itemId = Long.parseLong(element.select("[data-item-id]").attr("data-item-id"));

        String title = element.select("*[itemprop=\"url\"]").attr("title");
        String itemLink =  element.select("*[itemprop=\"url\"]").attr("href");
        String imageLink =  element.select("*[itemprop=\"image\"]").attr("srcset");
        Integer price = Integer.parseInt(element.select("*[itemprop=\"price\"]").attr("content"));

        Item item = new Item();
        item.setItemId(itemId);
        item.setTitle(title);
        item.setImageLink(imageLink);
        item.setItemLink(itemLink);
        item.setPrice(price);
        item.setSavedTime(LocalDateTime.now());

        return item;
    }
}
