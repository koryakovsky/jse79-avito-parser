package jse77.sample.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class SearchService {

    private final AvitoService avitoService;

    @Autowired
    public SearchService(AvitoService avitoService) {
        this.avitoService = avitoService;
    }

    @Scheduled(fixedRate = 10 * 60 * 1000)
    public void proceeding() throws Exception {
        avitoService.proceedSearchItems();
    }
}
